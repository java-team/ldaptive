Source: ldaptive
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Joseph Nahmias <jello@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 maven-debian-helper (>= 2.6),
Build-Depends-Indep:
 libcodemodel-java,
 libgoogle-gson-java,
 liblogback-java,
 libmaven-bundle-plugin-java (>= 3.5.1),
 libmaven-dependency-plugin-java (>= 3.5.0),
 libnetty-java,
 libslf4j-java (>= 1.7.32),
 libspring-context-java,
 testng,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/ldaptive.git
Vcs-Browser: https://salsa.debian.org/java-team/ldaptive
Homepage: http://www.ldaptive.org
Rules-Requires-Root: no

Package: libldaptive-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: simple, extensible Java API for interacting with LDAP servers
 Ldaptive is a simple, extensible Java API for interacting with LDAP
 servers. It was designed to provide easy LDAP integration for application
 developers. Features include:
 .
   * Netty based asynchronous networking
   * Reactive API
   * Connection pooling
   * Authentication API with support for password policy
   * JAAS modules for authentication and authorization
   * SSL/startTLS support with easy configuration of trust and key material
   * Input/output of LDIF
   * Supported controls:
     * Authorization Identity (RFC 3829)
     * Content Synchronization (RFC 4533)
     * Entry Change Notification (draft-ietf-ldapext-psearch-03)
     * ManageDsaIT (RFC 3296)
     * Matched Values (RFC 3876)
     * Paged Results (RFC 2696)
     * Password Policy (draft-behera-ldap-password-policy-10 and
       draft-vchu-ldap-pwd-policy-00)
     * Persistent Search (draft-ietf-ldapext-psearch-03)
     * Proxy Authorization (RFC 4370)
     * Server Side Sorting (RFC 2891)
     * Session Tracking (draft-wahl-ldap-session-03)
     * Tree Delete (draft-armijo-ldap-treedelete)
     * Virtual List View (draft-ietf-ldapext-ldapv3-vlv-09)
